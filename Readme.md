# WEATHER FORCASTING APPLICATION #

* This is the part of the webtips asignment. The app is built using html,css,and js in frontend and NodeJS, Express as backend. The website is hosted using glitch.me
* The app consists of three secions.
* [Website Link](https://weathertodayy.glitch.me)

### TOP SECTION - WEATHER INFORMATION FOR THE SELECTED CITY 

* The realtime information like temperature, precipitation and humidity for the selected city from the dropdown is displayed along with the predicted weather condition for the next 4 hours with respect to the current time.

 ![WEATHER INFORMATION FOR THE SELECTED CITY](Assets/Output/top.png)


### MIDDLE SECTION - WEATHER CONDITION FOR THE LISTED CITIES

* The List of Cities will be displayed based on the weather(Sunny, Rainy or Snowy) preferred by the user. The Number of Cities to be Displayed can also be changed byb the user.

 ![WEATHER CONDITION FOR THE LISTED CITIES](Assets/Output/middle.png)


### BOTTOM SECTION - CONTINENT-WISE WEATHER CONDITION FOR THE LIST OF CITIES

* The List of Cities will be displayed based on the Continent Location. The Cities will be grouped based on Continent and they can be sorted by user preference and that subgroup can be sorted based on temperature.

 ![WEATHER INFORMATION FOR THE SELECTED CITY](Assets/Output/bottom.png)

### REQUIRED MODULES ###

* express - 4.17.1
* http - 0.0.1

```
npm install express
npm install http

```
## WEB APPLICATION SCREENSHOT ##
![WEATHER -Today](Assets/Output/Weather - TODAY.png)